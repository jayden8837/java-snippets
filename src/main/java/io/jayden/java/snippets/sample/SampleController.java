package io.jayden.java.snippets.sample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("sample")
@RestController
public class SampleController {

    @GetMapping
    public String sample() {
        return "sample";
    }
}
